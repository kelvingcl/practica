<?php

namespace ContainerJqk6rAM;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder13023 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer19f75 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties83c78 = [
        
    ];

    public function getConnection()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'getConnection', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'getMetadataFactory', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'getExpressionBuilder', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'beginTransaction', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'getCache', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->getCache();
    }

    public function transactional($func)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'transactional', array('func' => $func), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->transactional($func);
    }

    public function commit()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'commit', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->commit();
    }

    public function rollback()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'rollback', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'getClassMetadata', array('className' => $className), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'createQuery', array('dql' => $dql), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'createNamedQuery', array('name' => $name), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'createQueryBuilder', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'flush', array('entity' => $entity), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'clear', array('entityName' => $entityName), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->clear($entityName);
    }

    public function close()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'close', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->close();
    }

    public function persist($entity)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'persist', array('entity' => $entity), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'remove', array('entity' => $entity), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'refresh', array('entity' => $entity), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'detach', array('entity' => $entity), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'merge', array('entity' => $entity), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'getRepository', array('entityName' => $entityName), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'contains', array('entity' => $entity), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'getEventManager', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'getConfiguration', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'isOpen', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'getUnitOfWork', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'getProxyFactory', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'initializeObject', array('obj' => $obj), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'getFilters', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'isFiltersStateClean', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'hasFilters', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return $this->valueHolder13023->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer19f75 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder13023) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder13023 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder13023->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, '__get', ['name' => $name], $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        if (isset(self::$publicProperties83c78[$name])) {
            return $this->valueHolder13023->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder13023;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder13023;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder13023;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder13023;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, '__isset', array('name' => $name), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder13023;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder13023;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, '__unset', array('name' => $name), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder13023;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder13023;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, '__clone', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        $this->valueHolder13023 = clone $this->valueHolder13023;
    }

    public function __sleep()
    {
        $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, '__sleep', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;

        return array('valueHolder13023');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer19f75 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer19f75;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer19f75 && ($this->initializer19f75->__invoke($valueHolder13023, $this, 'initializeProxy', array(), $this->initializer19f75) || 1) && $this->valueHolder13023 = $valueHolder13023;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder13023;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder13023;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
